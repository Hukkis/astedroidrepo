package com.hukkis.spaceshooter.Managers;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.Preferences;

/**
 * Created by Eetu on 12.3.2018.
 * Works as a score manager class.
 * Has methods for adding and returning score.
 * Will add score based on a timer.
 * New high scores will be added to preferences file.
 *
 */

public class ScoreManager {

    int currentScore = 0;
    private double scoreTimer = 5;
    private Preferences prefs = Gdx.app.getPreferences("Highscore");



    public void addScore(int score)
    {
        currentScore += score;
    }

    public int getCurrentScore()
    {
        return currentScore;
    }

    public void update(float deltatime)
    {

        if(scoreTimer >= 10)
        {
            addScore(50);
            scoreTimer = 0;
        }

        scoreTimer += deltatime;
    }

    public void setHighScore()
    {
        if(currentScore > prefs.getInteger("Highscore"))
        {
            prefs.putInteger("Highscore", currentScore);
            prefs.flush();
        }
    }

    public int getHighScore()
    {
        return prefs.getInteger("Highscore");
    }

}
