package com.hukkis.spaceshooter.Managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Eetu on 12.3.2018.
 * Create a new font from .ttf file to be used in a on screen text.
 * Color and scaling is set here.
 * This class can provide with many different sizes and colors if needed.
 * a GlyphLayout is created to properly set the text on the screen.
 */

public class TextManager {

    FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("atarifont.ttf"));
    FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
    BitmapFont gameFont, titleFont;
    GlyphLayout layout;

    public TextManager()
    {
        parameter.size = 18;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        gameFont = generator.generateFont(parameter);
        layout = new GlyphLayout();
        // Color cyan
        gameFont.setColor(0,1,1,1);
        gameFont.getData().setScale(2,2);

        titleFont = generator.generateFont(parameter);
        layout = new GlyphLayout();
        // Color orange
        titleFont.setColor(1f,0.62f,0.0f,1);
        titleFont.getData().setScale(4,4);

    }

    public BitmapFont getGameFont()
    {
        return gameFont;
    }

    public BitmapFont getTitleFont() { return titleFont; }

    public void dispose()
    {
        gameFont.dispose();
        titleFont.dispose();
    }

}
