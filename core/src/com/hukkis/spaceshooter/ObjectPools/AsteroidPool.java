package com.hukkis.spaceshooter.ObjectPools;

import com.badlogic.gdx.utils.Pool;
import com.hukkis.spaceshooter.Actors.Asteroid;

/**
 * Created by Eetu on 11.3.2018.
 * Pool container that holds the poolable Asteroid objects.
 */

public class AsteroidPool extends Pool<Asteroid> {

    // Construct the pool with an initial amount and a max amount of objects.
    public AsteroidPool(int initial, int max)
    {
        super(initial, max);
    }


    @Override
    protected Asteroid newObject() {
        return new Asteroid();
    }
}
