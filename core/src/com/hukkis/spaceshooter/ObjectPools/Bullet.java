package com.hukkis.spaceshooter.ObjectPools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by Eetu on 11.3.2018.
 * Poolable bullet class.
 */

public class Bullet extends Actor implements Pool.Poolable {

    private int bulletSpeed = 950;

    Sprite sprite = new Sprite(new Texture(Gdx.files.internal("bullet.png")));
    private Rectangle rectangle;

    public Bullet()
    {
        setBounds(sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight());
        rectangle = new Rectangle(getX(), getY(), sprite.getWidth(), sprite.getHeight());
    }



    // Reset the bullet object
    @Override
    public void reset() {

       setPosition(0 -sprite.getWidth(), 0 -sprite.getHeight());
       rectangle.setPosition(0 -sprite.getWidth(), 0 -sprite.getHeight());

    }

    public void update(float deltatime)
    {
        setPosition(getX(),getY() + bulletSpeed * deltatime);
        rectangle.setPosition(getX(),getY());
        positionChanged();
    }

    public void fireBullet(float posX, float posY)
    {
        setPosition(posX,posY);
    }

    @Override
    protected void positionChanged()
    {
        super.positionChanged();
        sprite.setPosition(getX(),getY());
    }

    @Override
    public void draw(Batch batch, float parentAlpha)
    {
        sprite.draw(batch);
    }

    public Rectangle getRectangle()
    {
        return rectangle;
    }

}
