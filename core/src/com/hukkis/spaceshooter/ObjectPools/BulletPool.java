package com.hukkis.spaceshooter.ObjectPools;

import com.badlogic.gdx.utils.Pool;

/**
 * Created by Eetu on 11.3.2018.
 * Pool container that holds the poolable bullet objects.
 */

public class BulletPool extends Pool<Bullet> {


    // Construct the pool with an initial amount and a max amount of objects.
    public BulletPool(int initial, int max)
    {
        super(initial, max);
    }

    @Override
    protected Bullet newObject() {
        return new Bullet();
    }
}
