package com.hukkis.spaceshooter.ObjectPools;

import com.badlogic.gdx.utils.Pool;
import com.hukkis.spaceshooter.Actors.Droid;

/**
 * Created by Eetu on 12.3.2018.
 * Pool container that holds the poolable Droid objects.
 */

public class DroidPool extends Pool<Droid> {


    public DroidPool(int initial, int max)
    {
        super(initial,max);
    }

    @Override
    protected Droid newObject() {
        return new Droid();
    }
}
