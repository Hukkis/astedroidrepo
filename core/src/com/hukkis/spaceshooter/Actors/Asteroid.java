package com.hukkis.spaceshooter.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;


import java.util.Random;

/**
 * Created by Eetu on 6.3.2018.
 * Asteroid class.
 * Simple enemy type that moves between a random spawn point and
 * a random destination.
 */

public class Asteroid extends Enemy{

    Random randomGenerator;


    public Asteroid()
    {        
        sprite = new Sprite(new Texture(Gdx.files.internal("asteroid01.png")));

        originalSpeed = 180;
        maxLives = 2;
        scoreOnDeath = 100;
        currentLives = maxLives;

        // Random values for speed, spawnPoint and destination.
        randomGenerator = new Random();

        // Vector defining the spawning position of the Asteroid.
        spawnPos = new Vector2(randomGenerator.nextInt(Gdx.graphics.getWidth() - 1) + 1, Gdx.graphics.getHeight() + sprite.getHeight());

        // Vector defining the destination point.
        destination = new Vector2(randomGenerator.nextInt(Gdx.graphics.getWidth() - 1) + 1,0);

        // Direction of the Asteroid. Destination - spawnPosition vector normalized.
        direction = new Vector2(destination.sub(spawnPos).nor());

        // Set the current position to match the spawn position.
        setPosition(spawnPos.x,spawnPos.y);

        // Set bounds and a rectangle working as a collider.
        setBounds(sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight());
        rectangle = new Rectangle(getX(),getY(),sprite.getWidth(),sprite.getHeight());
    }


    // Update the position towards the destination with direction vector.
    @Override
    public void update(float deltatime)
    {

        setPosition(getX() + (direction.x * 2 * currentSpeed * deltatime), getY() + (direction.y * 2 * currentSpeed * deltatime));
        super.update(deltatime);

    }


    @Override
    public void spawn()
    {
        setPosition(randomGenerator.nextInt(Gdx.graphics.getWidth() - 1) + 1, Gdx.graphics.getHeight());
        destination.set(randomGenerator.nextInt(Gdx.graphics.getWidth() - 1) + 1, - 50);
        currentSpeed = originalSpeed + randomGenerator.nextInt(150);
        spawnPos.set(getX(), getY());
        direction.set(destination.sub(spawnPos).nor());
        rectangle.setPosition(getX(),getY());
    }

    @Override
    public void reset()
    {
        setPosition(0 -sprite.getWidth(), 0 -sprite.getHeight());
        rectangle.setPosition(getX(),getY());
        currentLives = maxLives;
        currentSpeed = originalSpeed;
    }



}
