package com.hukkis.spaceshooter.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;


/**
 * Created by Eetu on 12.3.2018.
 * An enemy which moves with wave-like pattern towards the bottom
 * of the screen.
 */

public class Droid extends Enemy{

    Random randomGenerator;

    TextureRegion[] animationDyingFrames;
    Animation<TextureRegion> animationDying;
    boolean playOnDeathAnimation = false;

    // To be used as helpers to get the animation positions right before resetting.
    float animPosX, animPosY;


    public Droid()
    {
        initializeAnimations();

        sprite = new Sprite(new Texture(Gdx.files.internal("droid.png")));

        originalSpeed = 180;
        maxLives = 3;
        scoreOnDeath = 400;
        currentLives = maxLives;

        // Random values for speed and spawnPoint.
        randomGenerator = new Random();

        // Vector defining the spawning position of the Asteroid.
        spawnPos = new Vector2(randomGenerator.nextInt(Gdx.graphics.getWidth() - 1) + 1, Gdx.graphics.getHeight() + sprite.getHeight());

        // Set the current position to match the spawn position.
        setPosition(spawnPos.x,spawnPos.y);

        // Set bounds and a rectangle working as a collider.
        setBounds(sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight());
        rectangle = new Rectangle(getX(),getY(),sprite.getWidth(),sprite.getHeight());

    }

    public void initializeAnimations()
    {
        Texture explosionSheet = new Texture("explosionsheet.png");
        TextureRegion[][] tempFrames = TextureRegion.split(explosionSheet, 64,64);

        animationDyingFrames = new TextureRegion[11];

        int index = 0;

        for(int i = 0; i < 11; ++i)
        {
            for(int j = 0; j < 1; j++)
            {
                animationDyingFrames[index++] = tempFrames[j][i];
            }

        }
        assert (animationDyingFrames != null);

        animationDying = new Animation(1f/20f,animationDyingFrames);
    }



    // Update the position towards the destination with direction vector.
    @Override
    public void update(float deltatime)
    {
        setPosition( getX() + -(float)Math.cos(getY() / 75) * 3, getY() - currentSpeed * deltatime );
        super.update(deltatime);

        if(getCurrentLives() <= 0)
        {
            animPosX = getX();
            animPosY = getY();
            playOnDeathAnimation = true;
        }

    }


    @Override
    public void spawn()
    {
        setPosition(randomGenerator.nextInt((Gdx.graphics.getWidth() - 100) - 100) + 100, Gdx.graphics.getHeight());
        currentSpeed = originalSpeed + randomGenerator.nextInt(100);
        spawnPos.set(getX(), getY());
        rectangle.setPosition(getX(),getY());
    }

    @Override
    public void reset()
    {
        setPosition(0 -sprite.getWidth(), 0 -sprite.getHeight());
        rectangle.setPosition(getX(),getY());
        currentLives = maxLives;
        currentSpeed = originalSpeed;
    }

    @Override
    public void draw(Batch batch, float parentAlpha)
    {
        super.draw(batch,parentAlpha);

        if(playOnDeathAnimation)
        {
            elapsedAnimationTime += Gdx.graphics.getDeltaTime();
            batch.draw(animationDying.getKeyFrame(elapsedAnimationTime,false), animPosX, animPosY);
             if(animationDying.isAnimationFinished(elapsedAnimationTime) == true)
             {
                 playOnDeathAnimation = false;
                 elapsedAnimationTime = 0;
             }

            Gdx.app.log("POS", ""+ animPosY + animPosY);
        }
    }

}
