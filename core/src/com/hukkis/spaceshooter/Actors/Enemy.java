package com.hukkis.spaceshooter.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Pool;


/**
 * Created by Eetu on 7.3.2018.
 * Works as a baseclass for all enemies.
 * extends Actor so that all enemies can be added on stage.
 * Has basic attributes for enemies, so that it is easier
 * to create new enemy types.
 */

public class Enemy extends Actor implements Pool.Poolable{


    Rectangle rectangle;
    Vector2 spawnPos;
    Vector2 destination;
    Vector2 direction;
    Sprite sprite;
    int currentSpeed;
    int originalSpeed;
    int currentLives;
    int maxLives;
    int scoreOnDeath;
    float changeColorTimer = 0;

    float elapsedAnimationTime = 0;


    public Enemy()
    {
    }

    public void update(float deltatime)
    {

        rectangle.setPosition(getX(),getY());
        positionChanged();

        changeColorTimer -= deltatime;
        if(changeColorTimer < 0)
            changeColorTimer = 0;

    }

    public void takeDamage()
    {
        if(currentLives <= 0)
            isDestroyed();
        currentLives --;

        changeColorTimer = 0.2f;

    }

    public void isDestroyed()
    {

    }


    @Override
    protected void positionChanged()
    {
        super.positionChanged();
        sprite.setPosition(getX(),getY());
    }

    public void spawn()
    {
    }

    public void initializeAnimations()
    {

    }

    @Override
    public void draw(Batch batch, float parentAlpha)
    {
        sprite.draw(batch);

        if(changeColorTimer != 0)
        {
            float changeColorTimer = (System.currentTimeMillis() % 500) / 500f;
            sprite.setColor(1- changeColorTimer,changeColorTimer,changeColorTimer, 1);
            sprite.draw(batch);
            sprite.setColor(1,1,1,parentAlpha);
        }
    }

    @Override
    public void reset() {
    }

    public Rectangle getRectangle() { return rectangle; }

    public int getCurrentLives()
    {
        return currentLives;
    }

    public int getScoreOnDeath()
    {
        return scoreOnDeath;
    }

}
