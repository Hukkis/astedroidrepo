package com.hukkis.spaceshooter.Actors;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Eetu on 6.3.2018.
 */

public class Text extends Actor {

    BitmapFont font;

    public Text()
    {
        font = new BitmapFont();
        font.setColor(0,1,0,1);
    }


    public void draw(SpriteBatch batch, float parentAlpha)
    {
        font.draw(batch, "Lives: ", 10,10);
    }
}
