package com.hukkis.spaceshooter.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.hukkis.spaceshooter.Actors.Asteroid;
import com.hukkis.spaceshooter.Actors.Droid;
import com.hukkis.spaceshooter.Actors.Enemy;
import com.hukkis.spaceshooter.Actors.SpaceShip;
import com.hukkis.spaceshooter.Effects.Explosion;
import com.hukkis.spaceshooter.Managers.BackgroundManager;
import com.hukkis.spaceshooter.Managers.ScoreManager;
import com.hukkis.spaceshooter.Managers.TextManager;
import com.hukkis.spaceshooter.ObjectPools.AsteroidPool;
import com.hukkis.spaceshooter.ObjectPools.Bullet;
import com.hukkis.spaceshooter.ObjectPools.BulletPool;
import com.hukkis.spaceshooter.ObjectPools.DroidPool;

import java.util.ArrayList;
import java.util.Random;


/**
 * Created by Eetu on 28.2.2018.
 * A game state, which contains the game play itself.
 * This class has a list of different variables, like managers
 * and object pools.
 * Can be extended to another playstate, if more game levels
 * are needed.
 */

public class PlayState extends State {

    // Variables for the pausemenu
    private Table pauseMenuTable;

    private Music backgroundMusic,startSound,firingSound, explosionSound ;

    // Buttons.
    private Skin buttonSkin = new Skin(Gdx.files.internal("Data/glassy-ui.json"));
    private TextButton restartButton ,menuButton, continueButton,onGameMenuButton;


    // The player.
    private SpaceShip spaceShip;
    private float firingTimer;
    private boolean readyToFire;



    // Score calculator.
    private ScoreManager scoreManager;

    // Explosion effect array.
    private ArrayList<Explosion> explosions = new ArrayList<Explosion>();

    private Texture lifeTexture;

    // Object pool and array for bullets.
    private final Array<Bullet> activeBullets = new Array<Bullet>();
    private final BulletPool bulletPool = new BulletPool(30,50);

    // Object pool and array for asteroids.
    private final Array<Asteroid> activeAsteroids = new Array<Asteroid>();
    private final AsteroidPool asteroidPool = new AsteroidPool(15, 20);

    private final Array<Droid> activeDroids = new Array<Droid>();
    private final DroidPool droidPool = new DroidPool(5,10);


    double spawnTimer = 0;
    double randomSpawnTimer;
    Random randomTime = new Random();


    // Create the game and run initializing methods.
    public PlayState(GameStateManager gsm) {
        super(gsm);

        Paused = false;

        spaceShip = new SpaceShip(600,100);
        stage.addActor(spaceShip);

        // assetManager = new AssetManager();
        // assetManager.load("SpaceJam.mp3", Music.class);

        initializeMusic();
        initializeMenu();
        initializeUI();

    }


    // Update the status of all enemies, handle text updating
    // and increase timers.
    @Override
    public void update(float deltatime) {

        // Check if the game is paused. If it is, don't update.
        if(!Paused)
        {
            spaceShip.update(deltatime);
            handleInput();
            checkCollisions();
            scoreManager.update(deltatime);
            spawnRandomEnemy();

            // Update all asteroids and check if the enemy is out of the screen.
            for (Asteroid a : activeAsteroids) {

                a.update(deltatime);

                // If the asteroid is off screen destroy it
                if(a.getY() < -20)
                {
                    removeAsteroid(a);
                }
                if(a.getCurrentLives() <= 0)
                {
                    explosions.add(new Explosion((a.getX() + a.getWidth() / 2), (a.getY() + a.getHeight() / 2)));
                    explosionSound.play();
                    removeAsteroid(a);
                    scoreManager.addScore(a.getScoreOnDeath());
                }
            }

            for (Droid d : activeDroids) {

                d.update(deltatime);

                if(d.getY() < -20)
                {
                    removeDroid(d);
                }
                if(d.getCurrentLives() <= 0)
                {
                    //explosions.add(new Explosion((d.getX() + d.getWidth() / 2), (d.getY() + d.getHeight() / 2)));
                    explosionSound.play();
                    removeDroid(d);
                    scoreManager.addScore(d.getScoreOnDeath());

                }
            }

            // Update bullets and check their position.
            for (Bullet bullet : activeBullets)
            {
                bullet.update(deltatime);

                if(bullet.getY() > Gdx.graphics.getHeight())
                {
                    bulletPool.free(bullet);
                    activeBullets.removeValue(bullet,true);
                    addAction(Actions.removeActor());
                }
            }

            if(firingTimer >= spaceShip.getFiringSpeed())
            {
                readyToFire = true;
                firingTimer = 0;
            }


            spawnTimer += deltatime;
            firingTimer += deltatime;
        }

        // Check if there are any explosions happening. If there are,
        // update their particles. Out of the pauseloop.
        if(explosions.size() > 0)
        {
            for(int i = 0; i < explosions.size(); ++i)
            {
                explosions.get(i).update(deltatime);

                if(explosions.get(i).timer <= 0)
                {
                    explosions.get(i).dispose();
                    explosions.remove(i);
                }

            }
        }
    }

    @Override
    public void render(SpriteBatch spritebatch) {

        stage.act();
        stage.draw();


        spritebatch.begin();
        backgroundManager.renderBackground(spritebatch);

        for(int i = 0; i < spaceShip.getLives(); ++i)
            spritebatch.draw(lifeTexture, lifeTexture.getWidth() * i + 15, Gdx.graphics.getHeight() - 75);

        textLayout.setText(font,"Score: " + scoreManager.getCurrentScore());
        font.draw(spritebatch, textLayout, ((Gdx.graphics.getWidth() - textLayout.width) / 2),Gdx.graphics.getHeight() - textLayout.height);


        // Render explosions
        for(int i = 0; i < explosions.size(); ++i)
            explosions.get(i).renderParticles(spritebatch);


        spritebatch.end();

    }

    // Check collisions between the spaceship and all enemies.
    private void checkCollisions()
    {
        for (Asteroid a : activeAsteroids) {

            // Check if the spaceship collides with an enemy.
            if(spaceShip.getRectangle().overlaps(a.getRectangle()) && spaceShip.getInvinsible() == false)
            {
                removeAsteroid(a);
                spaceShip.takeDamage();
                explosions.add(new Explosion((spaceShip.getX() + spaceShip.getWidth() / 2), (spaceShip.getY() + spaceShip.getHeight() / 2)));

                if(spaceShip.getLives() == 0)
                {
                    gameOver();
                    backgroundMusic.stop();
                    spaceShip.addAction(Actions.removeActor());
                    Paused = true;
                }
                // make sound
            }
            checkBulletCollision(a);
        }

        for (Droid d : activeDroids) {

            if(spaceShip.getRectangle().overlaps(d.getRectangle()) && spaceShip.getInvinsible() == false)
            {
                removeDroid(d);
                spaceShip.takeDamage();
                explosions.add(new Explosion((spaceShip.getX() + spaceShip.getWidth() / 2), (spaceShip.getY() + spaceShip.getHeight() / 2)));

                if(spaceShip.getLives() == 0)
                {
                    gameOver();
                    backgroundMusic.stop();
                    spaceShip.addAction(Actions.removeActor());
                    Paused = true;
                }
                // make sound
            }
            checkBulletCollision(d);
        }
    }


    // Loop all active bullets and see if they collide with an enemy.
    // If it does, run enemy's takedamage function
    // and remove the bullet from the stage and active array.
    private void checkBulletCollision(Enemy e)
    {
        for (Bullet bullet : activeBullets)
        {
            if(bullet.getRectangle().overlaps(e.getRectangle()))
            {
                e.takeDamage();
                bulletPool.free(bullet);
                activeBullets.removeValue(bullet,true);
                bullet.reset();
                addAction(Actions.removeActor());
            }
        }
    }

    // Spawn a random enemy based on a random value.
    // Currently 1/5 to spawn a Droid and a 4/5 chance to spawn an asteroid.
    private void spawnRandomEnemy()
    {

        if(spawnTimer >= randomSpawnTimer)
        {
            int index = randomTime.nextInt(5-1) +1;

            if(index == 1)
                spawnDroid();
            else
                spawnAsteroid();

            spawnTimer = 0;
            randomSpawnTimer = 0.2 + (1-0.2) * randomTime.nextDouble();
        }
    }

    // Obtain an asteroid from pool, run it's spawn method and add on stage.
    private void spawnAsteroid()
    {
        Asteroid a = asteroidPool.obtain();
        a.spawn();
        activeAsteroids.add(a);
        stage.addActor(a);

    }

    // Remove an asteroid from the stage, free it back to the pool and remove from active array.
    private void removeAsteroid(Asteroid asteroid)
    {
        asteroidPool.free(asteroid);
        activeAsteroids.removeValue(asteroid,true);
        asteroid.reset();
        addAction(Actions.removeActor());
    }

    // Obtain a droid from pool, run it's spawn method and add on stage.
    private void spawnDroid()
    {
        Droid d = droidPool.obtain();
        d.spawn();
        activeDroids.add(d);
        stage.addActor(d);
    }

    // Remove a droid from the stage, free it back to the pool and remove from active array.
    private void removeDroid(Droid droid)
    {
        droidPool.free(droid);
        activeDroids.removeValue(droid,true);
        droid.reset();
        addAction(Actions.removeActor());
    }


    // Run when player dies.
    private void gameOver()
    {
        showDeathMenu();
        scoreManager.setHighScore();
    }

    @Override
    protected void handleInput() {

        if(Gdx.input.justTouched() && readyToFire)
        {
            firingSound.play();
            Bullet b = bulletPool.obtain();
            b.fireBullet(spaceShip.getX() + spaceShip.getWidth() / 2,spaceShip.getY() + spaceShip.getHeight());
            activeBullets.add(b);
            stage.addActor(b);
            readyToFire = false;
        }
    }

    // Creating the UI, including text components and UI sprites.
    private void initializeUI()
    {
        lifeTexture = new Texture("heart.png");
        backgroundManager = new BackgroundManager();
        scoreManager = new ScoreManager();
        onGameMenuButton = new TextButton("Menu",buttonSkin);
        onGameMenuButton.setSize(300,100);
        onGameMenuButton.setPosition(Gdx.graphics.getWidth() - onGameMenuButton.getWidth(), Gdx.graphics.getHeight() - onGameMenuButton.getHeight());

        onGameMenuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                showPauseMenu();
                backgroundMusic.pause();
            }
        });

        stage.addActor(onGameMenuButton);
    }

    // Initialize the menu buttons and add listeners to them.
    // Organized in a table layout
    private void initializeMenu()
    {

        Gdx.input.setInputProcessor(stage);
        restartButton = new TextButton("Restart", buttonSkin);
        continueButton = new TextButton("Continue", buttonSkin);
        menuButton = new TextButton("Main Menu", buttonSkin);

        // Define the table layout.

        pauseMenuTable = new Table();
        pauseMenuTable.setFillParent(true);
        pauseMenuTable.setWidth(1920);
        pauseMenuTable.setHeight(1080);

        pauseMenuTable.add(continueButton).fillX().uniformX();
        pauseMenuTable.row().pad(10,0,10,0);
        pauseMenuTable.add(restartButton).fillX().uniformX();
        pauseMenuTable.row().pad(10,0,10,0);
        pauseMenuTable.add(menuButton).fillX().uniformX();
        pauseMenuTable.row().pad(10,0,10,0);


        // Listeners for buttons.
        restartButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                restart();
            }
        });

        menuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                changeState(0);
            }
        });

        continueButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Paused = false;
                backgroundMusic.play();
                pauseMenuTable.addAction(Actions.removeActor());
            }
        });

    }

    // Load all audio files to make sure they are ready when played.
    // Play the starting sound and start looping the backgroundmusic.
    private void initializeMusic()
    {
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("SpaceJam.mp3"));
        startSound = Gdx.audio.newMusic(Gdx.files.internal("EngineStart.wav"));

        firingSound = Gdx.audio.newMusic(Gdx.files.internal("gunShot.wav"));
        firingSound.setVolume(0.2f);

        explosionSound = Gdx.audio.newMusic(Gdx.files.internal("Explosion.wav"));
        explosionSound.setVolume(0.5f);

        startSound.play();
        startSound.setVolume(0.5f);
        startSound.setLooping(false);
        backgroundMusic.play();
        backgroundMusic.setLooping(true);
        backgroundMusic.setVolume(0.3f);

        startSound.setOnCompletionListener(new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music music) {
                backgroundMusic.setVolume(0.5f);
                Gdx.app.log("music complete", "true");
            }
        });
    }

    private void showPauseMenu()
    {
        Paused = true;
        stage.addActor(pauseMenuTable);
    }

    private void showDeathMenu()
    {
        Paused = true;
        pauseMenuTable.removeActor(pauseMenuTable.getChildren().first());
        stage.addActor(pauseMenuTable);
    }

    // Restart the game without loading a new scene.
    // Reset all the positions and score values.
    public void restart()
    {
        Paused = false;

        backgroundMusic.stop();
        initializeMusic();

        spaceShip.setLives(spaceShip.getMaxLives());
        scoreManager.addScore(- scoreManager.getCurrentScore());

        pauseMenuTable.addAction(Actions.removeActor());

        for (Asteroid a: activeAsteroids) {
            removeAsteroid(a);
        }

        for (Droid d: activeDroids)
        {
            removeDroid(d);
        }

        for (Bullet b : activeBullets)
        {
            bulletPool.free(b);
            activeBullets.removeValue(b,true);
        }

        stage.addActor(spaceShip);

    }


    @Override
    public void dispose() {
        stage.clear();
        stage.dispose();
        backgroundManager.dispose();
        lifeTexture.dispose();
        buttonSkin.dispose();
        startSound.dispose();
        explosions.clear();
        asteroidPool.clear();
        droidPool.clear();
        bulletPool.clear();
        //backgroundMusic.dispose();
        System.gc();
    }
}
