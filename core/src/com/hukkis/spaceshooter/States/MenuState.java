package com.hukkis.spaceshooter.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.hukkis.spaceshooter.Managers.BackgroundManager;
import com.hukkis.spaceshooter.Managers.TextManager;

import javax.xml.soap.Text;

/**
 * Created by Eetu on 18.2.2018.
 *
 * A menu state that holds UI buttons.
 * Player can start or exit the game.
 * Current High score will be displayed.
 */

public class MenuState extends State {


    private Table menuTable;

    private Skin playButtonSkin = new Skin(Gdx.files.internal("Data/glassy-ui.json"));
    private TextButton playButton;
    private TextButton exitButton;
    private GlyphLayout title;

    public MenuState(GameStateManager gsm){

        super(gsm);
        // Enable this stage to take user input.
        // Add actors.


        initializeUI();
        initializeMenu();

    }

    @Override
    protected void handleInput() {
        // empty for now
    }


    @Override
    public void render(SpriteBatch spritebatch) {

        stage.draw();
        spritebatch.begin();
        backgroundManager.renderBackground(spritebatch);
        font.draw(spritebatch, textLayout, (Gdx.graphics.getWidth() - textLayout.width) / 2,Gdx.graphics.getHeight() - 300);
        font.draw(spritebatch, title, (Gdx.graphics.getWidth() - title.width) / 2, Gdx.graphics.getHeight() - title.height);
        spritebatch.end();
    }

    // Disposing of textures.
    @Override
    public void dispose()
    {
        backgroundManager.dispose();
        playButtonSkin.dispose();
        stage.dispose();
        font.dispose();
        titleFont.dispose();
        System.gc();
    }

    // Initialize menu table and buttons.
    private void initializeMenu()
    {
        // Initialize textures, buttons etc.
        playButton = new TextButton("Play", playButtonSkin);
        exitButton = new TextButton("Exit", playButtonSkin);

        // Table to hold all the UI buttons.
        // Define table layout.
        menuTable = new Table();
        menuTable.setFillParent(true);
        menuTable.setWidth(1920);
        menuTable.setHeight(1080);

        menuTable.add(playButton).fillX().uniformX();
        menuTable.row().pad(10,0,10,0);
        menuTable.add(exitButton).fillX().uniformX();
        menuTable.row().pad(10,0,10,0);

        stage.addActor(menuTable);


        // Listeners for buttons.
        playButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                changeState(1);
                dispose();
            }
        });

        exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                Gdx.app.exit();
            }
        });
    }

    // Initialize UI text and fonts.
    private void initializeUI()
    {
        Gdx.input.setInputProcessor(stage);

        backgroundManager = new BackgroundManager();

        titleFont = textManager.getTitleFont();

        textLayout.setText(font,"Highscore: " + prefs.getInteger("Highscore"));

        title = new GlyphLayout(titleFont, "ASTEDROID");
    }

    @Override
    public void update(float deltatime) {}

}
